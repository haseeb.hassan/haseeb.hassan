### Hello there 👋

[![GitHub thehaseebhassan](https://img.shields.io/github/followers/hacceebhassan?label=follow&style=social)](https://github.com/thehaseebhassan)


```python
#Introduction

class Developer:

    def __init__(self):
        self.name = "M. Haseeb Hassan"
        self.role = "Sr. Data Engineer"
        self.language_spoken = ["English", "Urdu", "Italian"]

    def say_hi(self):
        print("If you want to bounce off ideas on AI, ML, NLP, etc. Contact me :)")


me = Developer()
me.say_hi()
```

---
